//
//  AlbumTableViewCell.swift
//  AlbumProject
//
//  Created by user on 6/28/19.
//  Copyright © 2019 Razeware. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var userId: UILabel!
    @IBOutlet private weak var Id: UILabel!
    @IBOutlet private weak var title: UILabel!
    
    func setCell(_ album: Model) {
        userId.text = "Album Id: " + String(album.Id)
        Id.text = "User Id: " + String(album.userId)
        title.text = album.title
    }
}
    extension NSObject {
        static var className: String {
            return String(describing: self)
        
}
}
